﻿# Projekt Zespołowy #
Konfiguracja gita
http://krakiewicz.pl/git-bitbucket-tortoisegit-puttygen-konfiguracja-import-projektu-clone-repository
## Metody Numeryczne ##
1. Szereg Fouriera - Mariusz Milewczyk
2. Całkowanie metodą trapezów / całkowanie metodą kwadratów - Bartłomiej Romanek
3. Całkowanie metodą Simpsona - Radosław Tomaszewski
4. Metoda Najmniejszych kwadratów , Metoda Eulera lub Ekstrapolacja iterowana Richardsona  - Marta Gęza
5. Bisekcja, Netwona-Raphson, Metoda siecznych - Fabiola Kwasowiec
## Inne Metody ##
- Szereg Fouriera 
- Interpolacja(metoda Langranga)   
- Bisekcja, Netwona-Raphson, Metoda siecznych   
- Metoda Eliminacji Gaussa ( wyświetlanie krok po  kroku)  
- Metoda Najmniejszych kwadratów , Metoda Eulera lub Ekstrapolacja iterowana Richardsona  
- Całkowanie Numeryczne ( Prostokątów, metoda Trapezów)  
- Całkowanie Monte-Carlo  
- Całkowanie metodą simpsona  
- (hard) Dyskretna Transformata Fouriera
## Generacja obrazków i wzorów do teorii (latex) ##
http://rogercortesi.com/eqn/index.php   
https://www.matematyka.pl/28951.htm  
http://magnum34.pl/  
http://web.ift.uib.no/Teori/KURS/WRK/TeX/symALL.html   
## Biblioteczki do wykresów ##
http://www.android-graphview.org/  
https://github.com/PhilJay/MPAndroidChart  <- ten jest wykorzystany   
https://github.com/lecho/hellocharts-android 
### Biblioteczki ####
https://github.com/koral--/android-gif-drawable - do wczytania plików gif
## Kursy i książki ##
https://www.youtube.com/watch?v=9kHPZebqUMg&list=PLhf6FZHr6APR3vNOV0GadRBVUqfzXzTqR  
http://andrzejklusiewicz-android.blogspot.com/p/bezpatny-kurs-programowania-android-java.html  
### WERSJA BETA #####
v 1.0.0