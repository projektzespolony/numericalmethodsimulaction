package pl.group.simulaction.method.numericalmethodsimulation.methods;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.util.ArrayList;

import static android.graphics.Color.GREEN;
import static android.graphics.Color.RED;

import pl.group.simulaction.method.numericalmethodsimulation.R;

public class SimulationNewtonRapshon extends AppCompatActivity {
    private TextView a, b, step, result,l1, wsp1,wsp2,wsp3,wsp4,wsp5;
    private Button buttonSim;
    private static double x, xk = 5, xp = 0, n=2,nn=2;
    private int i;
    static int n2;
    private static int p,q;
    private static double tab[];
    static double l;
    static int k;
    static double y,z,c;

    private ArrayList<Entry> function = new ArrayList<Entry>();
    private ArrayList<Entry> integral = new ArrayList<Entry>();
    private ArrayList<String> labels1 = new ArrayList<String>();
    private ArrayList<String> labels2 = new ArrayList<String>();
    private ArrayList<LineDataSet> lines = new ArrayList<LineDataSet>();
   // private List<Iteration> iterations = new ArrayList<>();

    //	algorytm Hornera - obliczanie wartosci wielomianu
    private static double w(int k, double x) {
        if (k==nn)
            return tab[n2];
        else
            return w(k+1,x)*x+tab[k];
    }


    //	Algorytm Show-Trauba funkcja pomocnicza s(j)
    private static double s(int j)	{
        return (nn-j)%q;
    }

    //	Algorytm Show-Trauba funkcja pomocnicza r(j)
    private static double r(int j)	{
        if (j%q==0)
            return q;
        else
            return 0;
    }

    //	Algorytm Show-Trauba - glowna funkcja
    private static double T(int i, int j, double x) {
        if (x==0)   //by mozna bylo obliczyc pochodna w punkcie x=0
            return tab[j];
        else
        if (j==-1)

            return tab[n2-i-1]*Math.pow(x,s(i+1));
        else
        if (i==j)
            return tab[n2]*Math.pow(x,s(0));
        else
            return T(i-1, j-1, x)+T(i-1, j, x)*Math.pow(x,r(i-j));
    }


    private static double pochodna(int stopien, double punkt) {
        if (punkt==0)
            return T(n2,stopien,punkt);
        else
            return T(n2,stopien,punkt)/Math.pow(punkt,stopien%q);
    }

    public double f(double x){
        //return Math.sin(x);
        return tab[0]*x*x*x*x+tab[1]*x*x*x*x+tab[2]*x*x+tab[3]*x+tab[4];
    }

    private double[] linspace(double min, double max, int points) {
        double[] d = new double[points];
        for (int i = 0; i < points; i++) {
            d[i] = min + i * (max - min) / (points - 1);
        }

        return d;
    }

    public void simulation(){
        if(a.getText().toString().matches("") && b.getText().toString().matches("") && step.getText().toString().matches("")){
            xp = 0;
            xk = 5;
            n = 10;
            y = 0;
            z = 2;
            l = 8;
            nn=4;
            n2=(int) nn;
            tab = new double[n2+1];
            tab[0]=1;
            tab[1]=2;
            tab[2]=1;
            tab[3]=9;
            tab[4]=1;

        }
        else{
            xp = Double.parseDouble(a.getText().toString());
            xk = Double.parseDouble(b.getText().toString());
            n = Double.parseDouble(step.getText().toString());
            nn= 5;
            n2=(int) nn;
            tab = new double[n2+1];
            tab[0] = Double.parseDouble(wsp1.getText().toString());
            tab[1] = Double.parseDouble(wsp2.getText().toString());
            tab[2] = Double.parseDouble(wsp3.getText().toString());
            tab[3] = Double.parseDouble(wsp4.getText().toString());
            tab[4] = Double.parseDouble(wsp5.getText().toString());
            y = Double.parseDouble(a.getText().toString());
            z = Double.parseDouble(b.getText().toString());
            l = Double.parseDouble(l1.getText().toString());

        }

        p = 1;
        q = n2 + 1;
        function.clear();
        labels1.clear();
        if (pochodna(2,y)*w(0,y)>0) {
            c=y;
        }
        else {
            c=z;
        }
        for (k=1; k<=l; k++) {
            c=c-(w(0,c)/pochodna(1,c));
            function.add(new Entry((float)f(c),k));
            labels1.add(String.valueOf((c)));
            //function.add(new Entry((float)c,k));
            //labels1.add(String.valueOf((k*w(0,c))));
            if (w(0,c)==0) {
                break;
            }
        }

        if (w(0,c)==0) {

            result.setText(String.format( "Dokladny pierwiastek w przedziale %.2f:%.2f wynosi %.2f",y,z, c));
        }
        else {
            result.setText(String.format( "Przybliżony pierwiastek w przedziale %.2f:%.2f  wynosi %.2f",y,z, c));
        }

        // Configure LineChart

        double[] t = linspace(y - 2, z + 2, 100);
        LineChart chart = (LineChart) findViewById(R.id.chart);
        chart.setDescription("Wizualizacja metody Newtona-Rapshona.");

        LineDataSet dataSet = new LineDataSet(function, " ");

        dataSet.setColor(GREEN);

        lines.clear();
        lines.add(dataSet);

        LineData lineData = new LineData(labels1, lines);
        chart.setData(lineData);

        YAxis leftYAxis = chart.getAxisLeft();
        leftYAxis.setStartAtZero(false);

        chart.invalidate();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simulation_newton_rapshon);
        a = (TextView)findViewById(R.id.point_a);
        b = (TextView)findViewById(R.id.point_b);
        step = (TextView)findViewById(R.id.wspolczynnik_iteracji);
        result = (TextView) findViewById(R.id.simson_result);
        l1=(TextView) findViewById(R.id.wspolczynnik_iteracji);

        wsp1=(TextView) findViewById(R.id.wspolczynnik1);
        wsp2=(TextView) findViewById(R.id.wspolczynnik2);
        wsp3=(TextView) findViewById(R.id.wspolczynnik3);
        wsp4=(TextView) findViewById(R.id.wspolczynnik4);
        wsp5=(TextView) findViewById(R.id.wspolczynnik5);
        buttonSim = (Button) findViewById(R.id.simulation_start);

        simulation();
    }

    public void klikniecie(View v){
        simulation();
    }
}
