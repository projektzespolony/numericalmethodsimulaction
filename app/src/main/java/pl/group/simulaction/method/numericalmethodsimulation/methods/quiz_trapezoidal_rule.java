package pl.group.simulaction.method.numericalmethodsimulation.methods;

/* Trapezoidal rule - Quiz        */
/* Author: Bartłomiej Romanek     */
/* Last modification: 05-Jan-2016 */

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import pl.group.simulaction.method.numericalmethodsimulation.R;

public class quiz_trapezoidal_rule extends AppCompatActivity {
    private String[] questions = {
            "Metoda trapezów polega na przybliżaniu wartości pola nad wykresem funkcji za pomocą trapezów.",
            "Metoda trapezów pozwala na wyznaczenie całki nieoznaczonej za pomocą trapezów.",
            "Metoda trapezów jest jedną z najczęściej wykorzystywanych metod numerycznych.",
            "Metoda trapezów daje lepsze rezultaty dla mniejszego kroku całkowania.",
            "Metoda trapezów opiera się na wykorzystaniu kwadratur pierwszego stopnia.",
            "Im więcej zastosowanych węzłów w przedziale, tym dokładność wyniku większa.",
            "Optymalnym krokiem całkowania dla metody trapezów jest liczba h > 1."
    };

    private String[] answers = {"N", "N", "N", "Y", "Y", "Y", "N"};

    private TextView points;
    private TextView questionNumber;
    private TextView questionContent;

    private Button answerN;
    private Button answerY;

    private int iterator = 0;
    private int pointsInt = 0;

    AlertDialog alert;

    public void setQuestion() {
        points.setText(String.valueOf(pointsInt));

        if (iterator < 7) {
            questionNumber.setText(String.valueOf(iterator+1));
            questionContent.setText(questions[iterator]);
        }

        else {
            alert = new AlertDialog.Builder(quiz_trapezoidal_rule.this).create();
            alert.setMessage("Uzyskałeś " + String.valueOf(pointsInt) + " punktów!");
            alert.setTitle("Koniec gry!");
            alert.setButton(AlertDialog.BUTTON_NEUTRAL, "OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    // On alert 'OK' button go to theory page.
                    Intent temp = new Intent(quiz_trapezoidal_rule.this, theory_trapezoidal_rule.class);
                    startActivity(temp);
                }
            });
            alert.show();
        }
    }

    public void getAnswerNo(View v) {
        if (iterator < 7) {
            if (answers[iterator].compareTo("N") == 0) {
                //if (Objects.equals(answers[iterator], "N")) {
                pointsInt++;
            }
            iterator++;
        }
        setQuestion();
    }

    public void getAnswerYes(View v) {
        if (iterator < 7) {
            if (answers[iterator].compareTo("Y") == 0) {
                //if (Objects.equals(answers[iterator], "Y")) {
                pointsInt++;
            }
            iterator++;
        }
        setQuestion();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.quiz_trapezoidal_rule);

        points = (TextView)findViewById(R.id.textPoints);
        questionContent = (TextView)findViewById(R.id.textQuestionContent);
        questionNumber = (TextView)findViewById(R.id.textQuestionNumber);

        answerN = (Button)findViewById(R.id.buttonN);
        answerY = (Button)findViewById(R.id.buttonY);

        // Set first question
        setQuestion();
    }
}