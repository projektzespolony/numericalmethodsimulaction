package pl.group.simulaction.method.numericalmethodsimulation.methods;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Objects;

import pl.group.simulaction.method.numericalmethodsimulation.R;

public class QuizLeastSquares extends AppCompatActivity {

    private String[] questions =
            {
                    "Czy Metoda Najmniejszych Kwadratów w statystyce wykorzystywana jest do estymacji i wyznaczania linii trendu na podstawie zbioru danych w postaci par liczb?",
                    "Czy nazwa „najmniejsze kwadraty” oznacza, że końcowe rozwiązanie tą metodą minimalizuje sumę kwadratów błędów?",
                    "Czy Metoda Najmniejszych Kwadratów została wprowadzona przez Gauss’a w 1805?",
                    "Czy Metoda Najmniejszych Kwadratów opiera się tylko i wyłącznie na założeniu pewnego matematyka - Legendre’a, które nie wynika z żadnej ścisłej matematycznej teorii?",
                    "Czy Metoda Najmniejszych Kwadratów to najlepsza i matematycznie, najbardziej pewna metoda estymacji i wyznaczania linii trendu?",
            };

    private String[] answers = {"Y", "Y", "N", "Y", "N"};

    private TextView points;
    private TextView questionNumber;
    private TextView questionContent;

    private Button answerN;
    private Button answerY;

    private int iterator = 0;
    private int pointsInt = 0;

    public void setQuestion()
    {
        points.setText(String.valueOf(pointsInt));

        if (iterator < 5)
        {
            questionNumber.setText(String.valueOf(iterator+1));
            questionContent.setText(questions[iterator]);
        }
        else
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Uzyskales: " + pointsInt + " punktow")
                    .setTitle("Jeszcze raz?")
                    .setCancelable(false)
                    .setPositiveButton("Nie", new DialogInterface.OnClickListener()
                    {
                        public void onClick(DialogInterface dialog, int id)
                        {
                            QuizLeastSquares.this.finish();
                        }
                    })
                    .setNegativeButton("Tak", new DialogInterface.OnClickListener()
                    {
                        public void onClick(DialogInterface dialog, int id)
                        {
                            finish();
                            startActivity(getIntent());
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }
    }

    public void getAnswerNo(View v)
    {
        if (Objects.equals(answers[iterator], "N"))
        {
            pointsInt++;
        }
        iterator++;
        setQuestion();
    }

    public void getAnswerYes(View v)
    {
        if (Objects.equals(answers[iterator], "Y"))
        {
            pointsInt++;
        }
        iterator++;
        setQuestion();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_least_squares);

        points = (TextView)findViewById(R.id.textPoints);
        questionContent = (TextView)findViewById(R.id.textQuestionContent);
        questionNumber = (TextView)findViewById(R.id.textQuestionNumber);

        answerN = (Button)findViewById(R.id.buttonN);
        answerY = (Button)findViewById(R.id.buttonY);

        // Set first question
        setQuestion();
    }
}
