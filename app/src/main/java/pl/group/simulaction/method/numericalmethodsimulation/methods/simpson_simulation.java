package pl.group.simulaction.method.numericalmethodsimulation.methods;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Spinner;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.util.ArrayList;

import static android.graphics.Color.GREEN;
import static android.graphics.Color.RED;
import pl.group.simulaction.method.numericalmethodsimulation.R;
import pl.group.simulaction.method.numericalmethodsimulation.R;
public class simpson_simulation extends AppCompatActivity {
    private double x, xk = 5, xp = 0, dx, calka, s, n = 10;
    private int i;
    private double j;
    private TextView a, b, step, result;
    private Button buttonSim;
    private Spinner simpsonSpinner;

    private ArrayList<Entry> function = new ArrayList<Entry>();
    private ArrayList<Entry> integral = new ArrayList<Entry>();
    private ArrayList<String> labels1 = new ArrayList<String>();
    private ArrayList<String> labels2 = new ArrayList<String>();
    private ArrayList<LineDataSet> lines = new ArrayList<LineDataSet>();

    public double f(Double x) {
        if(simpsonSpinner.getSelectedItem().toString().equals("x^2"))
            return x * x;
        else if(simpsonSpinner.getSelectedItem().toString().equals("Sin(x)"))
            return Math.sin(x);
        else if(simpsonSpinner.getSelectedItem().toString().equals("Cos(x)"))
            return Math.cos(x);

        return 0;
    }

    public void simulation(){

        if(a.getText().toString().matches("") && b.getText().toString().matches("") && step.getText().toString().matches("")){
            xp = 0;
            xk = 5;
            n = 10;
        }
        else if(Double.parseDouble(step.getText().toString()) <= 2){
            AlertDialog alertDialog = new AlertDialog.Builder(simpson_simulation.this).create();
            alertDialog.setTitle("Alert");
            alertDialog.setMessage("Ilość kroków musi byc większa od 2");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();
        }
        else if(Double.parseDouble(a.getText().toString()) > Double.parseDouble(b.getText().toString())){
            AlertDialog alertDialog = new AlertDialog.Builder(simpson_simulation.this).create();
            alertDialog.setTitle("Alert");
            alertDialog.setMessage("Od musi być mniejsze od Do");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();
        }
        else{
            xp = Double.parseDouble(a.getText().toString());
            xk = Double.parseDouble(b.getText().toString());
            n = Double.parseDouble(step.getText().toString());
        }

        //Wykres calki
        dx = (xk - xp) / (float)n;
        integral.clear();
        labels1.clear();
        calka = 0;
        s = 0;
        for (i=1; i<n; i++) {
            x = xp + i*dx;
            integral.add(new Entry((float)f(x),i));
            labels1.add(String.valueOf((i*dx)));
            s += f(x - dx / 2);
            calka += f(x);
        }
        s += f(xk - dx / 2);


        calka = (dx/6) * (f(xp) + f(xk) + 2*calka + 4*s);
/*
        //wykres samej funkcji
        function.clear();
        labels2.clear();
        i = 1;
        for(j = 0; j < xk; j += 0.1){
            function.add(new Entry((float)f(j), i));
            labels2.add(String.valueOf(j));
            i++;
        }
*/
        result.setText(String.format( "Wartość całki sin(x), dla przedziału %.2f-%.2f: %.2f", xp, xk, calka));

        // Configure LineChart
        LineChart chart = (LineChart) findViewById(R.id.chart);
        chart.setDescription("Przybliżenie całki za pomocą paraboli.");

        LineDataSet dataSet = new LineDataSet(integral, "Przybliżenie("+simpsonSpinner.getSelectedItem().toString()+")");
        //LineDataSet apprSet = new LineDataSet(integral, simpsonSpinner.getSelectedItem().toString());

        dataSet.setColor(GREEN);
        //apprSet.setColor(RED);

        lines.clear();
        lines.add(dataSet);
        //lines.add(apprSet);

        LineData lineData = new LineData(labels1, lines);
        chart.setData(lineData);

        YAxis leftYAxis = chart.getAxisLeft();
        leftYAxis.setStartAtZero(false);

        chart.animateY(5000);
        chart.invalidate();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simpson_simulation);

        a = (TextView)findViewById(R.id.point_a);
        b = (TextView)findViewById(R.id.point_b);
        step = (TextView)findViewById(R.id.n_steps);
        result = (TextView) findViewById(R.id.simson_result);

        buttonSim = (Button) findViewById(R.id.simulation_start);

        simpsonSpinner = (Spinner) findViewById(R.id.simpsonSpinner);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.simpson_functions, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        simpsonSpinner.setAdapter(adapter);

        simulation();
    }

    public void klikniecie(View v){
        simulation();
    }
}
