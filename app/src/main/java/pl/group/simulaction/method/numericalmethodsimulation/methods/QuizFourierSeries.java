package pl.group.simulaction.method.numericalmethodsimulation.methods;

import android.graphics.Color;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import pl.group.simulaction.method.numericalmethodsimulation.R;

public class QuizFourierSeries extends AppCompatActivity implements View.OnClickListener {

    private TextView score;
    private Button check;
    private List<RadioGroup> Questions = new ArrayList<RadioGroup>();
    private List<Integer> Answers = new ArrayList<Integer>();
    private RadioButton radioAnswer;
    private List<CheckBox> QuestionsCheckBox = new ArrayList<CheckBox>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_fourier_series);
        score = (TextView) findViewById(R.id.score);
        check = (Button) findViewById(R.id.check);

        Questions.add((RadioGroup) findViewById(R.id.questionOne));
        Questions.add((RadioGroup) findViewById(R.id.questionTwo));
        Questions.add((RadioGroup) findViewById(R.id.questionThree));
        Questions.add((RadioGroup) findViewById(R.id.questionFour));
        Questions.add((RadioGroup) findViewById(R.id.questionFive));
        Questions.add((RadioGroup) findViewById(R.id.questionSeven));
        QuestionsCheckBox.add((CheckBox) findViewById(R.id.correct1Q6));
        QuestionsCheckBox.add((CheckBox) findViewById(R.id.correct2Q6));
        QuestionsCheckBox.add((CheckBox) findViewById(R.id.correct3Q6));
        QuestionsCheckBox.add((CheckBox) findViewById(R.id.correct4Q6));
        Answers.add(R.id.correctQ1);
        Answers.add(R.id.correctQ2);
        Answers.add(R.id.correctQ3);
        Answers.add(R.id.correctQ4);
        Answers.add(R.id.correctQ5);
        Answers.add(R.id.correctQ7);
        score.setVisibility(View.GONE);
        check.setOnClickListener(this);



    }

    @Override
    public void onClick(View v) {
        score.setVisibility(v.VISIBLE);
        int count = Questions.size() + QuestionsCheckBox.size();
        int points = 0;
        try {
            for (int i = 0; i < Questions.size(); i++) {
                int selectedId = Questions.get(i).getCheckedRadioButtonId();
                radioAnswer = (RadioButton) findViewById(selectedId);
                if (radioAnswer.getId() == Answers.get(i)) {
                    points++;
                }
            }
            for (int i = 0; i < QuestionsCheckBox.size(); i++) {
                if (QuestionsCheckBox.get(i).isChecked()) {
                    points++;
                }
            }
        }catch (Exception e){
            Toast.makeText(this," Nie zaznaczyłeś odpowiedzi" , Toast.LENGTH_SHORT).show();
        }

        float percentage = ((float) points / count) * 100;
        score.setTextColor(Color.WHITE);
        if (percentage < 50.0) {
            score.setBackgroundColor(Color.parseColor("#B71C1C"));
        } else if (percentage < 70.0) {
            score.setBackgroundColor(Color.parseColor("#F57C00"));
        } else {
            score.setBackgroundColor(Color.parseColor("#2E7D32"));
        }
        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);
        df.setMinimumFractionDigits(2);
        score.setText("Twój wynik to - " + df.format(percentage) + "%");

    }


}
