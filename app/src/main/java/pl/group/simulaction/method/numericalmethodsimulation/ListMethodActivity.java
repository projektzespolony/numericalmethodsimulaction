package pl.group.simulaction.method.numericalmethodsimulation;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.Toast;

import java.util.List;

public class ListMethodActivity extends AppCompatActivity {
    ExpandableListView expandableListView;
    ExpandableListAdapter expandableListAdapter;
    List<String> expandableListTitle;
    List<Object> expandableListItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_method);
        ListDataMethods dataList = new ListDataMethods();

        expandableListView = (ExpandableListView) findViewById(R.id.expandableListView);

        expandableListTitle = dataList.getGroupItem();
        expandableListItems = dataList.getChildItem();

        expandableListAdapter = new GroupAdapterList(ListMethodActivity.this,this, expandableListTitle, expandableListItems );
        expandableListView.setAdapter(expandableListAdapter);
        expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {

            }
        });

        expandableListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {


            }
        });
    }
}
