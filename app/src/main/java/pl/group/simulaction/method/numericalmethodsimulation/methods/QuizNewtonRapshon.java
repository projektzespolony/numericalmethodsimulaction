package pl.group.simulaction.method.numericalmethodsimulation.methods;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Objects;

import pl.group.simulaction.method.numericalmethodsimulation.R;

import static pl.group.simulaction.method.numericalmethodsimulation.R.id.buttonY;

public class QuizNewtonRapshon extends AppCompatActivity {
    private String[] questions = {
            "Metoda Newtona-Rapshona jest również zwana metodą stycznych",
            "Warunkiem jest, że pierwsza i druga pochodna zadanej funkcji mają stały znak w badanym przedziale <a ,b>.",
            "funkcja f oraz jej pierwsza i druga pochodna są ciągłe w badanym przedziale <a, b>",
            "W zadanym przedziale <a, b> znajdują się dokładnie dwa pierwiastki.",
            "Metoda Newtona-Rapshona jest również nazywana metodą Newtona",
            " "
    };

    private String[] answers = {"N", "N", "Y", "Y", "Y"};

    private TextView points;
    private TextView questionNumber;
    private TextView questionContent;

    private Button answerN;
    private Button answerY;

    private int iterator = 0;
    private int licznik=1;
    private int pointsInt = 0;
    private int textView7;

    public void setQuestion() {

        questionNumber.setText(String.valueOf(licznik));
        questionContent.setText(questions[iterator]);
        points.setText(String.valueOf(pointsInt));

    }

    public void getAnswerNo(View v) {
        if (Objects.equals(answers[iterator], "N")) {
            pointsInt++;
        }
        iterator++;
        licznik++;
        if(iterator !=5)
            setQuestion();
        else  {
            points.setText(String.valueOf(pointsInt)+"/5");
            questionContent.setText(questions[iterator]);
            Button button = (Button) findViewById(R.id.buttonY);
            Button button1 = (Button) findViewById(R.id.buttonN);
            button1.setVisibility(View.GONE);
            button.setVisibility(View.GONE);
            TextView t=(TextView)findViewById(R.id.textView7);
            t.setText("Twój całkowity wynik");
            questionNumber.setText(" ");
        }
    }

    public void getAnswerYes(View v) {
        if (Objects.equals(answers[iterator], "Y")) {
            pointsInt++;
        }
        iterator++;
        licznik++;
        if(iterator !=5)
            setQuestion();
        else  {points.setText(String.valueOf(pointsInt)+"/5");
            questionContent.setText(questions[iterator]);
            Button button = (Button) findViewById(R.id.buttonY);
            Button button1 = (Button) findViewById(R.id.buttonN);
            button1.setVisibility(View.GONE);
            button.setVisibility(View.GONE);
            TextView t=(TextView)findViewById(R.id.textView7);
            t.setText("Twój całkowity wynik");
            questionNumber.setText(" ");
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_newton_rapshon);
        points = (TextView)findViewById(R.id.textPoints);
        questionContent = (TextView)findViewById(R.id.textQuestionContent);
        questionNumber = (TextView)findViewById(R.id.textQuestionNumber);

        answerN = (Button)findViewById(R.id.buttonN);
        answerY = (Button)findViewById(buttonY);

        // Set first question
        setQuestion();
    }
}
