package pl.group.simulaction.method.numericalmethodsimulation.methods;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.lang.Math;
import java.text.*;
import java.util.Random;

import pl.group.simulaction.method.numericalmethodsimulation.R;

public class SimulactionFourierSeries extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener ,SeekBar.OnSeekBarChangeListener, View.OnClickListener {

    public static final int PLEASE_WAIT_DIALOG = 1;
    private SeekBar seekBar;
    private SeekBar seekBarRanget;
    private boolean typeGraph = false;
    private int t;
    Spinner spinner;
    TextView changeTextN;
    TextView changeTextt;
    EditText fromX;
    EditText toX;
    EditText amplitude;
    Button setting;
    Switch switchWihtout;

    private LineChart graph;
    private int N;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simulaction_fourier_series);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Wizualizacja Szeregu Fouriera");






        graph = (LineChart) findViewById(R.id.chart);
        //Wygląd wykresu
        YAxis left = graph.getAxisLeft();
        left.setAxisMinValue(-5.2f);
        left.setAxisMaxValue(5.2f);
        left.setStartAtZero(false);
        XAxis xAxis = graph.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);


        //Ustawienia
        setting = (Button) findViewById(R.id.accept);
        seekBar = (SeekBar) findViewById(R.id.seekBar);
        changeTextN = (TextView) findViewById(R.id.textViewChangeN);
        changeTextt = (TextView) findViewById(R.id.textViewChanget);
        seekBarRanget = (SeekBar) findViewById(R.id.seekBar_t);
        fromX = (EditText) findViewById(R.id.editTextFrom);
        toX = (EditText) findViewById(R.id.editTextTo);
        amplitude = (EditText) findViewById(R.id.editTextAmplitude);
        switchWihtout = (Switch) findViewById(R.id.switchWithoutN);
        spinner = (Spinner) findViewById(R.id.spinner);



        N = seekBar.getProgress();
        t = seekBarRanget.getProgress();
        //Nasłuchiwanie
        setting.setOnClickListener(this);
        seekBar.setOnSeekBarChangeListener(this);
        // spinner.setOnItemSelectedListener((AdapterView.OnItemSelectedListener) this);
        // spinner.setOnClickListener(this);
        seekBarRanget.setOnSeekBarChangeListener(this);
        changeTextN.setText(String.valueOf(N));
        changeTextt.setText(String.valueOf(t));


    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.simulaction_fourier_series, menu);
        return true;
    }
    @Override
    public Dialog onCreateDialog(int dialogId) {

        switch (dialogId) {
            case PLEASE_WAIT_DIALOG:
                ProgressDialog dialog = new ProgressDialog(this);
                dialog.setTitle("Obliczanie");
                dialog.setMessage("Proszę czekać....");
                dialog.setCancelable(true);
                Log.i(" type = ",Boolean.toString(switchWihtout.isChecked()));

                return dialog;

            default:
                break;
        }

        return null;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        int id = seekBar.getId();
        switch(id){
            case R.id.seekBar:
                if(progress > 0){
                    this.N = progress;
                }else{
                    this.N = 1;
                }
                changeTextN.setText(String.valueOf(N));
                break;
            case R.id.seekBar_t:
                if(progress > 10){
                    this.t = progress;
                }else{
                    this.t = 10;
                }
                changeTextt.setText(String.valueOf(t));
                break;
        }

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        //TODO: Empty
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        //TODO: Empty
    }

    @Override
    public void onClick(View v) {
        try{
            int a = Integer.parseInt(fromX.getText().toString());
            int b = Integer.parseInt(toX.getText().toString());
            double T = Double.parseDouble(amplitude.getText().toString());
            if ((b - a) < 2 || T < 0.5) {
                throw new Exception("Zbyt mały zakres lub zły zakres !!");
            }
            if(a > 1000 || b > 1000 || T > 100){
                throw new Exception("Zbyt duży zakres!!");
            }
            if(switchWihtout.isChecked()){
                this.typeGraph = true;
            }else{
                this.typeGraph = false;
            }
            new Obliczenia(this,graph,this.N,T,this.t,a,b,this.typeGraph,(int)this.spinner.getSelectedItemId()).execute();

        }catch (NumberFormatException ex){
            Toast.makeText(this, "Zbyt duży zakres lub wpisz liczbę!!", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }

    //Wykowanywanie obliczeń
    public class Obliczenia extends AsyncTask<Void, Void, Void> {

        Activity wywolujaceActivity;
        private LineChart graph;
        private int N = 1;
        private  double T = 0.5;
        private int function = 0;
        double[] t;
        private List<Double> fourierList = new ArrayList<Double>();
        private List<ArrayList<Double>> fourierListN = new ArrayList<ArrayList<Double>>();
        private List<Entry> yVals = new ArrayList<Entry>();
        private ArrayList<String> xVals = new ArrayList<String>();
        private boolean typeGraph;
        private int accurancy = 100;
        public Obliczenia(Activity wywolujaceActivity,LineChart graph,int n,double T, int t,int a,int b,boolean typeGraph,int function ) {
            this.wywolujaceActivity = wywolujaceActivity;
            this.graph = graph;
            this.N = n;
            this.t = Linspace(a,b,t);
            this.T = T;
            this.typeGraph = typeGraph;
            this.function = function;
        }
        public double[] Linspace(double min, double max, int points) {
            double[] d = new double[points];
            for (int i = 0; i < points; i++){
                d[i] = min + i * (max - min) / (points - 1);
            }
            return d;
        }
        private double Omega(double T)
        {
            return (2*Math.PI)/T;
        }
        public double f(double x){
            switch (this.function){
                case 0:
                    return 1;
                case 1:
                    return x;
                case 2:
                    return Math.abs(x);
                case 3:
                    return x*x;
                case 4:
                    return 2*x+3;
                case 5:
                    return 2*x+10;
                case 6:
                    return x*x+2*x+10;
                default:
                    return 1;
            }

            //return Math.sin(Math.PI*x) + Math.sin(2*Math.PI*x) + Math.sin(5*Math.PI*x);
        }
        public double f1(double x){
            switch (this.function){
                case 0:
                    return -1;
                default:
                    return 0;
            }

        }
        public double An(int n, double L,double T){
            double a = -L;
            double b = L;
            double[] x;
            double[] x1;
            double h = (b-a)/accurancy;
            double integration = 0;
            double integration2 = 0;
            if(this.function==0){
                x = Linspace(a,0,accurancy);
                x1 = Linspace(0, b, accurancy);

            }else if(this.function==1){
                x = Linspace(a,b,accurancy);
                x1 = Linspace(a, b, accurancy);
            }else {
                x1 = new double[]{ 0.0, 0.0};
                x = Linspace(a,b,accurancy);
            }


            for(int i = 0;i<x.length;i++){
                integration += f(x[i])*Math.cos((n*Math.PI*x[i])/L);
            }
            integration *= h;
            if (this.function == 0 && this.function == 1) {
                for (int i = 0; i < x1.length; i++) {
                    integration2 += f1(x[i]) * Math.cos(n * Omega(T) * x[i]);
                }
                integration2 *= h;
            }
            return (2 / T) * (integration + integration2);
        }
        public double Bn(int n, double L,double T) {
            double a = -L;
            double b = L;
            double[] x;
            double[] x1;
            double h = (b) / accurancy;
            double h1 = (a) / accurancy;
            double integration = 0;
            double integration2 = 0;
            if(this.function == 0){
                x = Linspace(a, 0, accurancy);
                x1 = Linspace(0, b, accurancy);
            }else if (this.function == 1) {
                x = Linspace(a, b, accurancy);
                x1 = Linspace(a, b, accurancy);
            } else {
                x1 = new double[]{ 0.0, 0.0};
                x = Linspace(a, b, accurancy);
            }
            for (int i = 0; i < x.length; i++) {
                integration += f(x[i]) * Math.sin(n * Omega(T) * x[i]);
            }
            integration *= h1;
            if (this.function == 0 && this.function == 1){

                for (int i = 0; i < x1.length; i++) {
                    integration2 += f1(x[i]) * Math.sin(n * Omega(T) * x[i]);
                }
                integration2 *= h;
            }

            return (2 / T) * (integration + integration2);
        }


        public void FourierSeries(int n,int t){

            double L = this.T/2;
            for(int i = 0;i<n;i++){
                ArrayList<Double> listN = new ArrayList<Double>();
                double a0 = 0;
                double sumN = 0;
                for (int j = 0; j < t; j++) {
                    sumN = a0 + ((An(i,L,this.T)*Math.cos(i*Omega(this.T)*this.t[j])) + (Bn(i,L,this.T)*Math.sin(i*Omega(this.T)*this.t[j])));
                    listN.add(sumN);
                }
                fourierListN.add(listN);
            }
            for(int i = 0;i < t; i++) {
                double a0 = 0;
                double sum = a0;
                for (int j = 1; j < n; j++) {

                    sum += ((An(j,L,this.T)*Math.cos(j*Omega(this.T)*this.t[i])) + (Bn(j,L,this.T)*Math.sin(j*Omega(this.T)*this.t[i])));
                    //sum = sum + Bn(j) * Math.sin(Wn(j) * x[i]);
                }
                fourierList.add(sum);
            }




        }



        @Override
        protected void onPreExecute() {
            wywolujaceActivity.showDialog(SimulactionFourierSeries.PLEASE_WAIT_DIALOG);
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            try {
                //TODO: wrzucenie metody Fourier Series
                FourierSeries(N,this.t.length);
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;

        }

        @Override
        protected void onPostExecute(Void result) {
            wywolujaceActivity.removeDialog(SimulactionFourierSeries.PLEASE_WAIT_DIALOG);
            Toast.makeText(wywolujaceActivity, "Obliczono!", Toast.LENGTH_SHORT).show();
            DecimalFormat df=new DecimalFormat();
            df.setMaximumFractionDigits(2);
            df.setMinimumFractionDigits(2);
            //this.GraphSum(df);

            if(this.typeGraph){
                this.GraphN(df);
            }else{
                this.GraphSum(df);
            }




        }
        public void GraphN(DecimalFormat df) {
            Random generator = new Random();
            List<LineDataSet> dataSets = new ArrayList<LineDataSet>();
            for (int j = 0; j < this.N; j++){
                xVals = new ArrayList<String>();
                yVals = new ArrayList<Entry>();
                for (int i = 0; i < this.t.length; i++) {
                    xVals.add(df.format(this.t[i]).toString());
                    yVals.add(new Entry(new Double(fourierListN.get(j).get(i)).floatValue(), i));
                }
                int n = j+1;
                LineDataSet signal = new LineDataSet(yVals,"n "+n);
                signal.setAxisDependency(YAxis.AxisDependency.LEFT);

                signal.setColor(Color.rgb(generator.nextInt(255), generator.nextInt(255), generator.nextInt(255)));
                dataSets.add(signal);

            }

            LineData linedata = new LineData(xVals,dataSets);
            this.graph.getLegend().setEnabled(false);
            this.graph.setData(linedata);
            this.graph.invalidate();
        }
        public void GraphSum(DecimalFormat df){
            for(int i = 0; i<this.t.length;i++){
                xVals.add(df.format(this.t[i]).toString());
                yVals.add(new Entry(new Double(fourierList.get(i)).floatValue(),i));
            }
            LineDataSet dataset = new LineDataSet(yVals,"Szereg Fouriera");
            dataset.setAxisDependency(YAxis.AxisDependency.LEFT);
            dataset.setColor(Color.rgb(0, 155, 0));
            LineData linedata = new LineData(xVals,dataset);
            this.graph.setData(linedata);
            this.graph.invalidate();
        }
    }
}