package pl.group.simulaction.method.numericalmethodsimulation;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckedTextView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import pl.group.simulaction.method.numericalmethodsimulation.methods.QuizFourierSeries;
import pl.group.simulaction.method.numericalmethodsimulation.methods.SimulactionFourierSeries;
import pl.group.simulaction.method.numericalmethodsimulation.methods.TheoryFourierSeries;
import pl.group.simulaction.method.numericalmethodsimulation.methods.simpson_theory;
import pl.group.simulaction.method.numericalmethodsimulation.methods.simpson_simulation;
import pl.group.simulaction.method.numericalmethodsimulation.methods.SimpsonQuizActivity;
import pl.group.simulaction.method.numericalmethodsimulation.methods.QuizBisection;
import pl.group.simulaction.method.numericalmethodsimulation.methods.QuizNewtonRapshon;
import pl.group.simulaction.method.numericalmethodsimulation.methods.SimulationBisection;
import pl.group.simulaction.method.numericalmethodsimulation.methods.SimulationNewtonRapshon;
import pl.group.simulaction.method.numericalmethodsimulation.methods.TheoryBisection;
import pl.group.simulaction.method.numericalmethodsimulation.methods.TheoryNewtonRapshon;
import pl.group.simulaction.method.numericalmethodsimulation.methods.theory_rectangual_rule;
import pl.group.simulaction.method.numericalmethodsimulation.methods.theory_trapezoidal_rule;
import pl.group.simulaction.method.numericalmethodsimulation.methods.theory_rectangual_rule;
import pl.group.simulaction.method.numericalmethodsimulation.methods.simulation_rectangual_rule;
import pl.group.simulaction.method.numericalmethodsimulation.methods.simulation_trapezoidal_rule;
import pl.group.simulaction.method.numericalmethodsimulation.methods.quiz_rectangual_rule;
import pl.group.simulaction.method.numericalmethodsimulation.methods.quiz_rectangual_rule;
import pl.group.simulaction.method.numericalmethodsimulation.methods.QuizEuler;
import pl.group.simulaction.method.numericalmethodsimulation.methods.QuizLeastSquares;
import pl.group.simulaction.method.numericalmethodsimulation.methods.SimulationMethodEuler;
import pl.group.simulaction.method.numericalmethodsimulation.methods.SimulationMethodLeastSquares;
import pl.group.simulaction.method.numericalmethodsimulation.methods.TheoryMethodEuler;
import pl.group.simulaction.method.numericalmethodsimulation.methods.TheoryMethodLeastSquares;

/**
 * Created by Magnum on 2016-10-21.
 */

public class GroupAdapterList extends BaseExpandableListAdapter {
    private Context context;
    private List<String> expandableListTitle, tempChild;
    private List<Object> expandableListDetail;
    public Activity activity;
    Intent intentMethodTheory;
    Intent intentMethodSimulaction;


    public GroupAdapterList(Context context, Activity act, List<String> expandableListTitle,
                                       List<Object> expandableListDetail) {
        this.context = context;
        this.activity = act;
        this.expandableListTitle = expandableListTitle;
        this.expandableListDetail = expandableListDetail;
    }

    @Override
    public Object getChild(int listPosition, int expandedListPosition) {
        return null;
    }

    @Override
    public long getChildId(int listPosition, int expandedListPosition) {
        return expandedListPosition;
    }

    @Override
    public View getChildView(final int groupPosition, final int expandedListPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        tempChild = (ArrayList<String>) expandableListDetail.get(groupPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.list_item, null);
        }
        TextView expandedListTextView = (TextView) convertView
                .findViewById(R.id.expandedListItem);
        expandedListTextView.setText(tempChild.get(expandedListPosition));
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(expandedListPosition == 0){
                    // TODO: goupPosition numer indeksu tablicy metod
                    switch(groupPosition){
                        case 0:
                            intentMethodTheory = new Intent(activity, TheoryBisection.class);
                            activity.startActivity(intentMethodTheory);
                            break;

                        case 1:
                            intentMethodTheory = new Intent(activity, TheoryNewtonRapshon.class);
                            activity.startActivity(intentMethodTheory);
                            break;
                        case 2:
                            intentMethodTheory = new Intent(activity, theory_rectangual_rule.class);
                            activity.startActivity(intentMethodTheory);
                            break;
                        case 3:
                            intentMethodTheory = new Intent(activity, theory_trapezoidal_rule.class);
                            activity.startActivity(intentMethodTheory);
                            break;
                        case 4:
                            intentMethodTheory = new Intent(activity, simpson_theory.class);
                            activity.startActivity( intentMethodTheory);
                            break;
                        case 5:
                            intentMethodTheory = new Intent(activity, TheoryMethodLeastSquares.class);
                            activity.startActivity(intentMethodTheory);
                            break;
                        case 6:
                            intentMethodTheory = new Intent(activity, TheoryMethodEuler.class);
                            activity.startActivity(intentMethodTheory);
                            break;
                        case 7:
                            intentMethodTheory = new Intent(activity, TheoryFourierSeries.class);
                            activity.startActivity(intentMethodTheory);
                            break;

                    }

                }else if(expandedListPosition == 1){
                    switch(groupPosition){
                        case 0:
                            intentMethodSimulaction = new Intent(activity, SimulationBisection.class);
                            activity.startActivity(intentMethodSimulaction);
                            break;
                        case 1:
                            intentMethodSimulaction = new Intent(activity, SimulationNewtonRapshon.class);
                            activity.startActivity(intentMethodSimulaction);
                            break;
                        case 2:
                            intentMethodSimulaction = new Intent(activity, simulation_rectangual_rule.class);
                            activity.startActivity(intentMethodSimulaction);
                            break;
                        case 3:
                            intentMethodSimulaction = new Intent(activity, simulation_trapezoidal_rule.class);
                            activity.startActivity(intentMethodSimulaction);
                            break;
                        case 4:
                            intentMethodSimulaction  = new Intent(activity, simpson_simulation.class);
                            activity.startActivity(intentMethodSimulaction);
                            break;
                        case 5:
                            intentMethodSimulaction = new Intent(activity, SimulationMethodLeastSquares.class);
                            activity.startActivity( intentMethodSimulaction);
                            break;
                        case 6:
                            intentMethodSimulaction = new Intent(activity, SimulationMethodEuler.class);
                            activity.startActivity( intentMethodSimulaction);
                            break;
                        case 7:
                            intentMethodSimulaction = new Intent(activity, SimulactionFourierSeries.class);
                            activity.startActivity(intentMethodSimulaction);
                            break;

                    }



                }else{
                    switch(groupPosition){
                        case 0:
                            intentMethodSimulaction = new Intent(activity, QuizBisection.class);
                            activity.startActivity(intentMethodSimulaction);
                            break;
                        case 1:
                            intentMethodSimulaction = new Intent(activity, QuizNewtonRapshon.class);
                            activity.startActivity(intentMethodSimulaction);
                            break;
                        case 2:
                            intentMethodSimulaction = new Intent(activity, quiz_rectangual_rule.class);
                            activity.startActivity(intentMethodSimulaction);
                            break;
                        case 3:
                            intentMethodSimulaction = new Intent(activity, quiz_rectangual_rule.class);
                            activity.startActivity(intentMethodSimulaction);
                            break;
                        case 4:
                            intentMethodSimulaction  = new Intent(activity, SimpsonQuizActivity.class);
                            activity.startActivity(intentMethodSimulaction);
                            break;
                        case 5:
                            intentMethodSimulaction  = new Intent(activity, QuizLeastSquares.class);
                            activity.startActivity(intentMethodSimulaction );
                            break;
                        case 6:
                            intentMethodSimulaction  = new Intent(activity, QuizEuler.class);
                            activity.startActivity(intentMethodSimulaction );
                            break;
                        case 7:
                            intentMethodSimulaction = new Intent(activity, QuizFourierSeries.class);
                            activity.startActivity(intentMethodSimulaction);
                            break;

                    }
                }
//                Toast.makeText(activity, "" + expandedListPosition,
//                        Toast.LENGTH_SHORT).show();

            }
        });
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return ((ArrayList<String>) expandableListDetail.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int listPosition) {
        return this.expandableListTitle.get(listPosition);
    }

    @Override
    public int getGroupCount() {
        return this.expandableListTitle.size();
    }

    @Override
    public long getGroupId(int listPosition) {
        return listPosition;
    }

    @Override
    public View getGroupView(int listPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String listTitle = (String) getGroup(listPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.list_group, null);
        }
        TextView listTitleTextView = (TextView) convertView
                .findViewById(R.id.listTitle);
        listTitleTextView.setTypeface(null, Typeface.BOLD);
        listTitleTextView.setText(listTitle);
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int listPosition, int expandedListPosition) {
        return true;
    }
}
