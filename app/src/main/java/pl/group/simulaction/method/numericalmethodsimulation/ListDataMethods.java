package pl.group.simulaction.method.numericalmethodsimulation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class ListDataMethods {
    private static String methods[] = {
            "Bisekcja",
            "Metoda Netwona-Raphsona",
            "Całkowanie metodą prostokątów",
            "Całkowanie metodą trapezów",
            "Całkowanie metodą Simpsona",
            "Metoda najmniejszych kwadratów",
            "Metoda Eulera",
            "Szereg Fouriera"
        };
    private List<String> groupItem = new ArrayList<String>();
    private List<Object> childItem = new ArrayList<Object>();

    public  ListDataMethods() {
        groupItem.addAll(Arrays.asList(ListDataMethods.methods));
        for(int i = 0;i< ListDataMethods.methods.length; i++){
            List<String> itemsChild = new ArrayList<String>();
            String[] item = {"Teoria","Symulacje","Quiz"};
            itemsChild.addAll(Arrays.asList(item));
            this.childItem.add(itemsChild);
        }


    }
    public List<String> getGroupItem(){
        return this.groupItem;
    }
    public List<Object> getChildItem(){
        return this.childItem;
    }
}
