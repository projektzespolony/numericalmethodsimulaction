package pl.group.simulaction.method.numericalmethodsimulation.methods;

/* Rectangual rule - Simulation   */
/* Author: Bartłomiej Romanek     */
/* Last modification: 05-Jan-2016 */

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.util.ArrayList;
import pl.group.simulaction.method.numericalmethodsimulation.R;

import static android.graphics.Color.GREEN;
import static android.graphics.Color.RED;

public class simulation_rectangual_rule extends AppCompatActivity {
    private TextView txtFrom;
    private TextView txtTo;
    private TextView txtStep;
    private Spinner rectangualSpinner;

    private int iterator, from, to;
    private double step;

    private ArrayList<Entry> function = new ArrayList<Entry>();
    private ArrayList<Entry> integral = new ArrayList<Entry>();
    private ArrayList<String> labels1 = new ArrayList<String>();
    private ArrayList<String> labels2 = new ArrayList<String>();
    private ArrayList<LineDataSet> lines = new ArrayList<LineDataSet>();

    public void runSimulation(View v) {
        if((txtFrom.getText().toString().trim().length() == 0) || (txtTo.getText().toString().trim().length() == 0) || (txtStep.getText().toString().trim().length() == 0)) {
            Toast.makeText(getApplicationContext(), "Uzupełnij wszystkie pola!", Toast.LENGTH_LONG).show();
            return;
        }

        if(Integer.parseInt(txtFrom.getText().toString().trim()) > Integer.parseInt(txtTo.getText().toString().trim())) {
            Toast.makeText(getApplicationContext(), "Nieprawidłowy zakres!", Toast.LENGTH_LONG).show();
            return;
        }

        from = Integer.parseInt(txtFrom.getText().toString());
        to = Integer.parseInt(txtTo.getText().toString());
        step = Double.parseDouble(txtStep.getText().toString());

        // Prepare data for LineChart: function
        iterator = 0;
        function.clear();
        labels1.clear();
        for(double i = from; i <= to; i=i+(step/20)) {
            if(rectangualSpinner.getSelectedItem().toString().equals("Sinus")) {
                function.add(new Entry((float)Math.sin(i), iterator));
            }

            else if(rectangualSpinner.getSelectedItem().toString().equals("Cosinus")) {
                function.add(new Entry((float)Math.cos(i), iterator));
            }

            else if(rectangualSpinner.getSelectedItem().toString().equals("Liniowa")) {
                function.add(new Entry((float)(i), iterator));
            }

            else {
                function.add(new Entry((float)(i*i), iterator));
            }

            labels1.add(String.valueOf(i));
            iterator++;
        }

        // Prepare data for LineChart: approximation
        iterator = 0;
        integral.clear();
        for(double i = from; i <= to; i=i+step) {
            if(rectangualSpinner.getSelectedItem().toString().equals("Sinus")) {
                integral.add(new Entry((float)((Math.sin(i)+(Math.sin(i+step)))/2), iterator*20));
                integral.add(new Entry((float)((Math.sin(i)+(Math.sin(i+step)))/2), (iterator+1)*20));
            }

            else if(rectangualSpinner.getSelectedItem().toString().equals("Cosinus")) {
                integral.add(new Entry((float)((Math.cos(i)+(Math.cos(i+step)))/2), iterator*20));
                integral.add(new Entry((float)((Math.cos(i)+(Math.cos(i+step)))/2), (iterator+1)*20));
            }

            else if(rectangualSpinner.getSelectedItem().toString().equals("Liniowa")) {
                integral.add(new Entry((float)(((i)+((i+step)))/2), iterator*20));
                integral.add(new Entry((float)(((i)+((i+step)))/2), (iterator+1)*20));
            }

            else {
                integral.add(new Entry((float)(((i*i)+((i+step)*(i+step)))/2), iterator*20));
                integral.add(new Entry((float)(((i*i)+((i+step)*(i+step)))/2), (iterator+1)*20));
            }

            iterator++;
        }

        // Prepare data for LineChart: approximation value
        double value = 0;
        for(double i = from; i <= to; i=i+step) {
            if(rectangualSpinner.getSelectedItem().toString().equals("Sinus")) {
                value += step*(Math.sin(i+(step/2)));
            }

            else if(rectangualSpinner.getSelectedItem().toString().equals("Cosinus")) {
                value += step*(Math.cos(i+(step/2)));
            }

            else if(rectangualSpinner.getSelectedItem().toString().equals("Liniowa")) {
                value += step*((i+(step/2)));
            }

            else {
                value += step*((i+(step/2))*(i+(step/2)));
            }
        }

        Toast.makeText(getApplicationContext(), "Przybliżona wartość całki: " + String.valueOf(value), Toast.LENGTH_LONG).show();

        // Configure LineChart
        LineChart chart = (LineChart) findViewById(R.id.chart);
        chart.setDescription("Przybliżenie całki za pomocą metody trapezów");

        YAxis leftYAxis = chart.getAxisLeft();
        leftYAxis.setStartAtZero(false);

        YAxis rightYAxis = chart.getAxisRight();
        rightYAxis.setDrawLabels(false);

        LineDataSet dataSet = new LineDataSet(function, rectangualSpinner.getSelectedItem().toString());
        dataSet.setDrawValues(false);
        dataSet.setDrawCircles(false);
        dataSet.setColor(GREEN);
        chart.clear();
        lines.clear();
        lines.add(dataSet);

        LineDataSet apprSet = new LineDataSet(integral, "Przybliżenie");
        apprSet.setDrawValues(false);
        apprSet.setDrawCircles(false);
        apprSet.enableDashedLine(1,1,1);
        apprSet.setDrawFilled(true);
        apprSet.setFillColor(RED);

        apprSet.setColor(RED);


        lines.add(apprSet);
        LineData lineData = new LineData(labels1, lines);
        chart.setData(lineData);
        chart.invalidate();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.simulation_rectangual_rule);

        txtFrom = (TextView)findViewById(R.id.rectangualFrom);
        txtTo = (TextView)findViewById(R.id.rectangualTo);
        txtStep = (TextView)findViewById(R.id.rectangualStep);

        // Configure spinner with function choice
        rectangualSpinner = (Spinner) findViewById(R.id.quadratureSpinner);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.trapezoidalFunctions, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        rectangualSpinner.setAdapter(adapter);
    }
}
