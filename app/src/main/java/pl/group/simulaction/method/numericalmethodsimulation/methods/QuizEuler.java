package pl.group.simulaction.method.numericalmethodsimulation.methods;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Objects;

import pl.group.simulaction.method.numericalmethodsimulation.R;

public class QuizEuler extends AppCompatActivity {
    private String[] questions =
    {
        "Czy Metoda Eulera jest inaczej zwana Metodą Siecznych?",
        "Czy wraz ze wzrostem wartości kroku w Metodzie Eulera, wzrasta również dokładność metody w przybliżaniu wyników funkcji?",
        "Czy metodę postępowania zgodnie z Metodą Eulera przedstawia wzór: \nyi+1=hyi+f(xi,yi), i=0,1,2,..., ?",
        "Dla ostatecznie małego h dominującym błędem jest błąd zaokrągleń. Czy dalsze zmniejszanie kroku może pogorszyć wyniki?\n",
        "Twórcą Metody Eulera jest Leonhard Euler. Czy to prawda, że podobizna Eulera widnieje na szwajcarskim banknocie 10-frankowym szóstej serii?",
    };

    private String[] answers = {"Y", "N", "N", "Y", "Y"};

    private TextView points;
    private TextView questionNumber;
    private TextView questionContent;

    private Button answerN;
    private Button answerY;

    private int iterator = 0;
    private int pointsInt = 0;

    public void setQuestion()
    {
        points.setText(String.valueOf(pointsInt));

        if (iterator < 5)
        {
            questionNumber.setText(String.valueOf(iterator+1));
            questionContent.setText(questions[iterator]);
        }
        else
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Uzyskales: " + pointsInt + " punktow")
                    .setTitle("Jeszcze raz?")
                    .setCancelable(false)
                    .setPositiveButton("Nie", new DialogInterface.OnClickListener()
                    {
                        public void onClick(DialogInterface dialog, int id)
                        {
                            QuizEuler.this.finish();
                        }
                    })
                    .setNegativeButton("Tak", new DialogInterface.OnClickListener()
                    {
                        public void onClick(DialogInterface dialog, int id)
                        {
                            finish();
                            startActivity(getIntent());
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }
    }

    public void getAnswerNo(View v)
    {
        if (Objects.equals(answers[iterator], "N"))
        {
            pointsInt++;
        }
        iterator++;
        setQuestion();
    }

    public void getAnswerYes(View v)
    {
        if (Objects.equals(answers[iterator], "Y"))
        {
            pointsInt++;
        }
        iterator++;
        setQuestion();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_euler);

        points = (TextView)findViewById(R.id.textPoints);
        questionContent = (TextView)findViewById(R.id.textQuestionContent);
        questionNumber = (TextView)findViewById(R.id.textQuestionNumber);

        answerN = (Button)findViewById(R.id.buttonN);
        answerY = (Button)findViewById(R.id.buttonY);

        // Set first question
        setQuestion();
    }
}
