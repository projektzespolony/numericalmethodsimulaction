package pl.group.simulaction.method.numericalmethodsimulation;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class AuthorActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_author);
        TextView txt = (TextView) findViewById(R.id.authorTitle);
        Typeface font = Typeface.createFromAsset(getAssets(), "Urban.ttf");
        txt.setTypeface(font);
        txt = (TextView) findViewById(R.id.author1);
        txt.setTypeface(font);
        txt = (TextView) findViewById(R.id.author2);
        txt.setTypeface(font);
        txt = (TextView) findViewById(R.id.author3);
        txt.setTypeface(font);
        txt = (TextView) findViewById(R.id.author4);
        txt.setTypeface(font);
        txt = (TextView) findViewById(R.id.author5);
        txt.setTypeface(font);
    }
}
