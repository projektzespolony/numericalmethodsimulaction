package pl.group.simulaction.method.numericalmethodsimulation.methods;

import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Objects;
import pl.group.simulaction.method.numericalmethodsimulation.R;

public class SimpsonQuizActivity extends AppCompatActivity {

    private String[] questions = {
            "Metoda Simpsona polega na przybliżaniu wartości pola pod wykresem funkcji za pomocą prostokątów.",
            "Metoda Simpsona opiera się na przybliżaniu funkcji całkowanej przez interpolację wielomianem drugiego stopnia.",
            "Metoda Simpsona pozwala na wyznaczenie całki nieoznaczonej za pomocą paraboli.",
            "Metoda Simpsona dzieli przedział całkowania na n równych części.",
            "Metoda Simpsona daje lepsze rezultaty dla większej ilości kroków.",
    };

    private String[] answers = {"N", "Y", "N", "Y", "Y"};

    private TextView points;
    private TextView questionNumber;
    private TextView questionContent;

    private Button answerN;
    private Button answerY;

    private int iterator = 0;
    private int pointsInt = 0;

    public void setQuestion() {
        points.setText(String.valueOf(pointsInt));
        if(iterator < 5){
            questionNumber.setText(String.valueOf(iterator));
            questionContent.setText(questions[iterator]);
        }
        else{
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Uzyskales: " + pointsInt + " punktow")
                    .setTitle("Jeszcze raz?")
                    .setCancelable(false)
                    .setPositiveButton("Nie", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            SimpsonQuizActivity.this.finish();
                        }
                    })
                    .setNegativeButton("Tak", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            finish();
                            startActivity(getIntent());
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }
    }

    public void getAnswerNo(View v) {
        if (Objects.equals(answers[iterator], "N")) {
            pointsInt++;
        }
        iterator++;
        setQuestion();
    }

    public void getAnswerYes(View v) {
        if (Objects.equals(answers[iterator], "Y")) {
            pointsInt++;
        }
        iterator++;
        setQuestion();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simpson_quiz);

        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        points = (TextView)findViewById(R.id.textPoints);
        questionContent = (TextView)findViewById(R.id.textQuestionContent);
        questionNumber = (TextView)findViewById(R.id.textQuestionNumber);

        answerN = (Button)findViewById(R.id.buttonN);
        answerY = (Button)findViewById(R.id.buttonY);

        // Set first question
        setQuestion();
    }
}
