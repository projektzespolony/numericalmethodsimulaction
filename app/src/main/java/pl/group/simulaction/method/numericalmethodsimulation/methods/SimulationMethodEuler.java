package pl.group.simulaction.method.numericalmethodsimulation.methods;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;
import java.util.List;

import pl.group.simulaction.method.numericalmethodsimulation.R;

public class SimulationMethodEuler extends AppCompatActivity{


    EditText pole_max_X;
    EditText pole_h;
    Button przycisk;
    int nr_funkcji;
    ImageView wzor_funkcji;
    ImageView wzor_funkcji_real;
    TextView miejsce_zerowe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simulation_method_euler);


        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.functions_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);

        wzor_funkcji = (ImageView) findViewById(R.id.wzor_funkcji);
        wzor_funkcji_real = (ImageView) findViewById(R.id.wzor_funkcji_real);
        miejsce_zerowe = (TextView) findViewById(R.id.miejsce_zerowe);
        pole_max_X = (EditText)findViewById(R.id.max_X);
        pole_h = (EditText)findViewById(R.id.h);
        przycisk = (Button) findViewById(R.id.button_euler);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                switch ((int)position)
                {
                    case 0:
                        nr_funkcji = 1;
                        wzor_funkcji.setImageResource(R.drawable.simulation_euler_2);
                        wzor_funkcji_real.setImageResource(R.drawable.simulation_euler_real_2);
                        miejsce_zerowe.setText("Miejsce zerowe funkcji to y(0) = 3");
                        pole_max_X.setText("2.5");
                        pole_h.setText("0.5");
                        break;
                    case 1:
                        nr_funkcji = 2;
                        wzor_funkcji.setImageResource(R.drawable.simulation_euler_3);
                        wzor_funkcji_real.setImageResource(R.drawable.simulation_euler_real_3);
                        miejsce_zerowe.setText("Miejsce zerowe funkcji to y(0) = 1");
                        pole_max_X.setText("4");
                        pole_h.setText("0.5");
                        break;
                    case 2:
                        nr_funkcji = 3;
                        wzor_funkcji.setImageResource(R.drawable.simulation_euler_4);
                        wzor_funkcji_real.setImageResource(R.drawable.simulation_euler_real_4);
                        miejsce_zerowe.setText("Miejsce zerowe funkcji to y(0) = 0");
                        pole_max_X.setText("12.56");
                        pole_h.setText("0.1");
                        break;
                    case 3:
                        nr_funkcji = 4;
                        wzor_funkcji.setImageResource(R.drawable.simulation_euler_5);
                        wzor_funkcji_real.setImageResource(R.drawable.simulation_euler_real_5);
                        miejsce_zerowe.setText("Miejsce zerowe funkcji to y(0) = 100");
                        pole_max_X.setText("25");
                        pole_h.setText("0.5");
                        break;
                    case 4:
                        nr_funkcji = 5;
                        wzor_funkcji.setImageResource(R.drawable.simulation_euler_6);
                        wzor_funkcji_real.setImageResource(R.drawable.simulation_euler_real_6);
                        miejsce_zerowe.setText("Miejsce zerowe funkcji to y(0) = 0");
                        pole_max_X.setText("5");
                        pole_h.setText("0.5");
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        przycisk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String s_max_X = pole_max_X.getText().toString();
                String s_h = pole_h.getText().toString();


                double max_X = Double.parseDouble(s_max_X);
                double h = Double.parseDouble(s_h);

                ArrayList<String> warosciX = new ArrayList<String>();

                List<Double> lista_X = new ArrayList<Double>();
                lista_X = pobierzWartosciX(0,max_X,h);
                Log.d("test 1",""+lista_X.size());
                warosciX = wykresOsX(lista_X);
                Log.d("test 1",""+warosciX.size());
                wyliczWartosciY(nr_funkcji, h, lista_X);

                LineDataSet dataset1 = new LineDataSet(warosciY, "Wartość funkcji dY/dX");
                dataset1.setColor(Color.rgb(0, 155, 0));
                LineDataSet dataset2 = new LineDataSet(warosciY_real, "Wartość rzeczywistej funkcji Y");
                dataset2.setColor(Color.rgb(0, 0, 155));


                ArrayList<LineDataSet> dataSets = null;
                dataSets = new ArrayList<>();
                dataSets.add(dataset1);
                dataSets.add(dataset2);

                LineChart chart = (LineChart) findViewById(R.id.ChartEuler);

                YAxis leftAxis = chart.getAxisLeft();
                YAxis rightAxis = chart.getAxisRight();

                if(nr_funkcji == 3)
                {
                    leftAxis.setAxisMinValue(-1.5f);
                    rightAxis.setAxisMinValue(-1.5f);
                    leftAxis.setAxisMaxValue(1.5f);
                    rightAxis.setAxisMaxValue(1.5f);

                    chart.getAxisLeft().setStartAtZero(false);
                    chart.getAxisRight().setStartAtZero(false);
                } else if(nr_funkcji == 2)
                    {
                        leftAxis.setAxisMaxValue(7.5f);
                        rightAxis.setAxisMaxValue(7.5f);

                        chart.getAxisLeft().setStartAtZero(false);
                        chart.getAxisRight().setStartAtZero(false);
                    }
                else if(nr_funkcji == 1)
                {
                    leftAxis.setAxisMaxValue(5.5f);
                    rightAxis.setAxisMaxValue(5.5f);

                    chart.getAxisLeft().setStartAtZero(false);
                    chart.getAxisRight().setStartAtZero(false);
                }
                else if(nr_funkcji == 4)
                {
                    chart.getAxisLeft().setStartAtZero(false);
                    chart.getAxisRight().setStartAtZero(false);
                }


                LineData data = new LineData(warosciX, dataSets);
                chart.setData(data);
                chart.setDescription("Matoda Eulera dla funkcji Y");
                chart.animateXY(2000, 2000);
                chart.invalidate();
            }
        });

    }

    public List<Double> pobierzWartosciX(double min_X, double max_X, double h)
    {

        double x0 = min_X;
        double x1 = x0;
        List<Double> lista_X = new ArrayList<Double>();
        do
        {
            lista_X.add(x1);
            x1 = x1 + h;
        }
        while (x1 <= max_X);

        return lista_X;
    }

    public ArrayList<String> wykresOsX(List<Double> lista_X)
    {
        ArrayList<String> warosciX = new ArrayList<String>();

        for (Double object: lista_X)
        {
            warosciX.add(String.format("%.2f", object));
        }
        return warosciX;
    }

    ArrayList<Entry> warosciY = new ArrayList<>();
    ArrayList<Entry> warosciY_real = new ArrayList<>();

    public double funkcja1(double y1, double h, List<Double> lista_X, int jeden)
    {

        if (jeden < lista_X.size())
        {

            double x = (double) lista_X.get(jeden-1);
            double a = -0.3*x;
            double b = Math.pow(Math.E,a);
            double c = 7*b;
            double d = -1.2*y1;
            double e = d+c;
            double f = h*e;
            double y2 = y1 + f;
            warosciY.add(new Entry((float) y2, jeden));
            jeden++;
            return funkcja1(y2, h, lista_X, jeden);
        }
        return y1;

    }

    public double funkcja1_real(List<Double> lista_X, int zero)
    {

        if (zero < lista_X.size())
        {

            double x = (double) lista_X.get(zero);
            double a1 = -0.3*x;
            double a2 = -1.2*x;
            double b1 = Math.pow(Math.E,a1);
            double b2 = Math.pow(Math.E,a2);
            double c1 = 70*b1/9;
            double c2 = 43*b2/9;
            double y = c1 - c2;
            warosciY_real.add(new Entry((float) y, zero));
            zero++;
            return funkcja1_real(lista_X, zero);
        }
        return zero;

    }

    public double funkcja2(double y1, double h, List<Double> lista_X, int jeden)
    {

        if (jeden < lista_X.size())
        {
            double x = (double) lista_X.get(jeden-1);
            double f = -2*Math.pow(x, 3)+12*Math.pow(x, 2)-20*x+8.5;
            double y2 = y1 + f*h;
            warosciY.add(new Entry((float) y2, jeden));
            jeden++;
            return funkcja2(y2, h, lista_X, jeden);
        }
        return y1;
    }

    public double funkcja2_real(List<Double> lista_X, int zero)
    {

        if (zero < lista_X.size())
        {
            double x = (double) lista_X.get(zero);
            double y = (-0.5*Math.pow(x, 4))+(4*Math.pow(x, 3))-(10*Math.pow(x,2))+(8.5*x)+1;
            warosciY_real.add(new Entry((float) y, zero));
            zero++;
            return funkcja2_real(lista_X, zero);
        }
        return zero;
    }

    public double funkcja3(double y1, double h, List<Double> lista_X, int jeden)
    {

        if (jeden < lista_X.size())
        {
            double x = (double) lista_X.get(jeden-1);
            double f = Math.cos(x);
            double y2 = y1 + f*h;
            warosciY.add(new Entry((float) y2, jeden));
            jeden++;
            return funkcja3(y2, h, lista_X, jeden);
        }
        return y1;
    }

    public double funkcja3_real(List<Double> lista_X, int zero)
    {

        if (zero < lista_X.size())
        {
            double x = (double) lista_X.get(zero);
            double y = Math.sin(x);
            warosciY_real.add(new Entry((float) y, zero));
            zero++;
            return funkcja3_real(lista_X, zero);
        }
        return zero;
    }

    public double funkcja4(double y1, double h, List<Double> lista_X, int jeden)
    {

        if (jeden < lista_X.size())
        {
            double x = (double) lista_X.get(jeden-1);
            double f = 2*(x-10);
            double y2 = y1 + f*h;
            warosciY.add(new Entry((float) y2, jeden));
            jeden++;
            return funkcja4(y2, h, lista_X, jeden);
        }
        return y1;
    }

    public double funkcja4_real(List<Double> lista_X, int zero)
    {

        if (zero < lista_X.size())
        {
            double x = (double) lista_X.get(zero);
            double y = Math.pow(x-10,2);
            warosciY_real.add(new Entry((float) y, zero));
            zero++;
            return funkcja4_real(lista_X, zero);
        }
        return zero;
    }

    public double funkcja5(double y1, double h, List<Double> lista_X, int jeden)
    {

        if (jeden < lista_X.size())
        {
            double x = (double) lista_X.get(jeden-1);
            double f = 1/(2*Math.sqrt(x));
            double y2 = y1 + f*h;
            warosciY.add(new Entry((float) y2, jeden));
            jeden++;
            return funkcja5(y2, h, lista_X, jeden);
        }
        return y1;
    }

    public double funkcja5_real(List<Double> lista_X, int zero)
    {

        if (zero < lista_X.size())
        {
            double x = (double) lista_X.get(zero);
            double y = Math.sqrt(x);
            warosciY_real.add(new Entry((float) y, zero));
            zero++;
            return funkcja5_real(lista_X, zero);
        }
        return zero;
    }

    public ArrayList<Entry> wyliczWartosciY(int nr_funkcji, double h, List<Double> lista_X)
    {
        switch (nr_funkcji)
        {
            case 1:
            {
                double y1 = 3;
                warosciY.clear();
                warosciY_real.clear();
                warosciY.add(new Entry((float) y1, 0));
                funkcja1(y1,h,lista_X,1);
                funkcja1_real(lista_X,0);
                break;
            }
            case 2:
            {
                double y1 = 1;
                warosciY.clear();
                warosciY_real.clear();
                warosciY.add(new Entry((float) y1, 0));
                funkcja2(y1,h,lista_X,1);
                funkcja2_real(lista_X,0);
                break;
            }
            case 3:
            {
                double y1 = 0;
                warosciY.clear();
                warosciY_real.clear();
                warosciY.add(new Entry((float) y1, 0));
                funkcja3(y1,h,lista_X,1);
                funkcja3_real(lista_X,0);
                break;
            }
            case 4:
            {
                double y1 = 100;
                warosciY.clear();
                warosciY_real.clear();
                warosciY.add(new Entry((float) y1, 0));
                funkcja4(y1,h,lista_X,1);
                funkcja4_real(lista_X,0);
                break;
            }
            case 5:
            {
                double y1 = 0;
                warosciY.clear();
                warosciY_real.clear();
                warosciY.add(new Entry((float) y1, 0));
                funkcja5(y1,h,lista_X,1);
                funkcja5_real(lista_X,0);
                break;
            }
        }

        return warosciY;
    }


}
