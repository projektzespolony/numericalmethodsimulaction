package pl.group.simulaction.method.numericalmethodsimulation;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import pl.group.simulaction.method.numericalmethodsimulation.methods.SimpsonQuizActivity;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Button buttonFinish = (Button) findViewById(R.id.finish);
        Button buttonAuthor = (Button) findViewById(R.id.author);
        Button buttonMethod = (Button) findViewById(R.id.method);
        buttonFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.super.finish();
            }
        });
        buttonAuthor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intentAuthor = new Intent(MainActivity.this, AuthorActivity.class);
                startActivity(intentAuthor);

               // Intent intentAuthor = new Intent(MainActivity.this, SimpsonQuizActivity.class);
              //  startActivity(intentAuthor );

            }
        });
        buttonMethod.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intentMethod = new Intent(MainActivity.this, ListMethodActivity.class);
                startActivity(intentMethod);
            }
        });

    }


}
