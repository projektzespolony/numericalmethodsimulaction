package pl.group.simulaction.method.numericalmethodsimulation.methods;

/* Trapezoidal rule - Theory      */
/* Author: Bartłomiej Romanek     */
/* Last modification: 05-Jan-2016 */

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import pl.group.simulaction.method.numericalmethodsimulation.R;

public class theory_trapezoidal_rule extends AppCompatActivity {

    public void goToSim(View v) {
        Intent trapSim = new Intent(this, simulation_trapezoidal_rule.class);
        startActivity(trapSim);
    }

    public void goToQuiz(View v) {
        Intent trapQuiz = new Intent(this, quiz_trapezoidal_rule.class);
        startActivity(trapQuiz);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.theory_trapezoidal_rule);
    }
}