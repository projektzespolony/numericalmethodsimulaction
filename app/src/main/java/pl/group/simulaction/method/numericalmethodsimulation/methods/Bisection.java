package pl.group.simulaction.method.numericalmethodsimulation.methods;

import android.util.Log;

import net.objecthunter.exp4j.Expression;

import java.util.ArrayList;
import java.util.List;

public class Bisection {

    private final Expression function;
    private final double e;

    private double a;
    private double b;

    private List<Iteration> iterations = new ArrayList<>();

    public Bisection(Expression function, double a, double b, double e) {
        this.function = function;
        this.a = a;
        this.b = b;
        this.e = e;
    }

    public static class Iteration {
        public double x1;
        public double x2;
        public double x3;
        public double fx1;
        public double fx2;
        public double fx3;
    }

    public double solve() {
        double x = 0;
        int k = 1;

        double dx = (b - a);

        validateFx1AndFx2Signs(a, b);

        while (Math.abs(dx) > e && k < 1000 && f(x) != 0) {
            Iteration it = new Iteration();
            log("Numer iteracji: " + k);
            log("x1:       " + a);
            log("x2:       " + b);
            log("f(x1):    " + f(a));
            log("f(x2):    " + f(b));
            it.x1 = a;
            it.x2 = b;
            it.fx1 = f(a);
            it.fx2 = f(b);

            x = ((a + b) / 2);

            if ((f(a) * f(x)) < 0) {
                b = x;
                dx = b - a;
            } else {
                a = x;
                dx = b - a;
            }

            k++;

            log("x3:       " + x);
            log("f(x3):    " + f(x));
            log("delta c:  " + dx);
            log("");
            it.x3 = x;
            it.fx3 = f(x);

            iterations.add(it);
        }

        log("Zatrzymano na iteracji numer " + (k - 1) + " ponieważ obliczona wartość" +
                "\ndelta c jest mniejsza niż podana tolereancja." +
                "\n\n*********************************************************");
        log("Numer iteracji: " + (k - 1));
        log("Otrzymany pierwiastek [x3] wynosi " + x);
        log("Wartość (x3) wynosi " + f(x));
        log("Delta x [dx] wynosi " + dx);

        return f(x);
    }

    public double f(double x) {
        return function.setVariable("x", x).evaluate();
    }

    public void validateFx1AndFx2Signs(double a, double b) {
        if (((f(a) >= 0) && (f(b) >= 0)) || ((f(a) < 0) && (f(b) < 0))) {
            throw new RuntimeException("Wartości f(a) i f(b) nie różnią się znakiem!");
        }
    }

    public List<Iteration> getIterations() {
        return this.iterations;
    }

    private void log(String msg) {
        Log.i("bisection", msg);
    }
}
