package pl.group.simulaction.method.numericalmethodsimulation.methods;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.FloatProperty;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.CombinedChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.CombinedData;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.ScatterData;
import com.github.mikephil.charting.data.ScatterDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


import pl.group.simulaction.method.numericalmethodsimulation.R;

import static java.lang.System.console;

public class SimulationMethodLeastSquares extends AppCompatActivity {

    private CombinedChart mChart;
    private Button add_button;
    private Button clear_button;
    private Button chart_button;
    private EditText wartosc_X;
    private EditText wartosc_Y;
    private TextView X1;
    private TextView X2;
    private TextView X3;
    private TextView X4;
    private TextView X5;
    private TextView Y1;
    private TextView Y2;
    private TextView Y3;
    private TextView Y4;
    private TextView Y5;
    private TextView wynik;

    int iterator = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simulation_method_least_squares);

        mChart = (CombinedChart) findViewById(R.id.ChartSquares);
        add_button = (Button) findViewById(R.id.add_button);
        clear_button = (Button) findViewById(R.id.clear_button);
        chart_button = (Button) findViewById(R.id.chart_button);
        wartosc_X = (EditText) findViewById(R.id.wartosc_X);
        wartosc_Y = (EditText) findViewById(R.id.wartosc_Y);
        final List<TextView> lista_X = new ArrayList<TextView>();
        final List<TextView> lista_Y = new ArrayList<TextView>();
        X1 = (TextView) findViewById(R.id.X1);
        lista_X.add(X1);
        X2 = (TextView) findViewById(R.id.X2);
        lista_X.add(X2);
        X3 = (TextView) findViewById(R.id.X3);
        lista_X.add(X3);
        X4 = (TextView) findViewById(R.id.X4);
        lista_X.add(X4);
        X5 = (TextView) findViewById(R.id.X5);
        lista_X.add(X5);
        Y1 = (TextView) findViewById(R.id.Y1);
        lista_Y.add(Y1);
        Y2 = (TextView) findViewById(R.id.Y2);
        lista_Y.add(Y2);
        Y3 = (TextView) findViewById(R.id.Y3);
        lista_Y.add(Y3);
        Y4 = (TextView) findViewById(R.id.Y4);
        lista_Y.add(Y4);
        Y5 = (TextView) findViewById(R.id.Y5);
        lista_Y.add(Y5);

        wynik = (TextView) findViewById(R.id.wynik);

        final List<List<Float>> listaXY = new ArrayList<List<Float>>();

        add_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (wartosc_X.getText().toString().isEmpty() || wartosc_Y.getText().toString().isEmpty())
                {
                    Toast.makeText(getApplicationContext(), "Podaj wartości X i Y", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    if(iterator < 5)
                    {
                        lista_X.get(iterator).setText(wartosc_X.getText().toString());
                        lista_Y.get(iterator).setText(wartosc_Y.getText().toString());

                        List<Float> row = new ArrayList<Float>();
                        listaXY.add(iterator,row);
                        listaXY.get(iterator).add(0, Float.parseFloat(wartosc_X.getText().toString()));
                        listaXY.get(iterator).add(1, Float.parseFloat(wartosc_Y.getText().toString()));

                        if(iterator < 4) iterator++;
                        else
                        {
                            Toast.makeText(getApplicationContext(), "Dodałeś już maksymalną liczbę zmiennych, możesz stworzyć wykres", Toast.LENGTH_LONG).show();
                        }
                    }
                }
                }
        });

        clear_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for(TextView object: lista_X)
                {
                    object.setText("");
                }
                for(TextView object: lista_Y)
                {
                    object.setText("");
                }

                listaXY.clear();
                iterator = 0;

            }
        });

        chart_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Collections.sort(listaXY, new Comparator<List<Float>>() {
                    @Override
                    public int compare(List<Float> wiersz1, List<Float> wiersz2) {
                        return wiersz1.get(0).compareTo(wiersz2.get(0));
                    }
                });

                mChart.setDrawOrder(new CombinedChart.DrawOrder[]
                        {CombinedChart.DrawOrder.SCATTER, CombinedChart.DrawOrder.LINE});



                ArrayList<String> warosciX = new ArrayList<String>();
                warosciX = wykresOsX(listaXY);
                CombinedData data = new CombinedData(warosciX);
                data.setData(generateScatterData(listaXY));
                data.setData(generateLineData(listaXY));

                mChart.setData(data);
                mChart.setDescription("Matoda Najmniejszych Kwadratów dla podanych wartości");
                mChart.animateXY(2000, 2000);

                iterator = 0;


            }
        });


    }

    private LineData generateLineData(List<List<Float>> listaXY) {

        ArrayList<Entry> entries = new ArrayList<Entry>();

        ArrayList<String> warosciX = new ArrayList<String>();
        warosciX = wykresOsX(listaXY);

        float size = listaXY.size();
        float delta, X_2, XY, X, Y;
        float A, B;

        X_2 = 0;
        XY = 0;
        X = 0;
        Y = 0;

        for(List<Float> object: listaXY)
        {
            X += object.get(0);
            Y += object.get(1);
            XY += object.get(0)*object.get(1);
            X_2 += object.get(0)*object.get(0);
        }

        delta = size*X_2 -(float)Math.pow(X, 2);
        B = (size*XY - Y*X)/delta;
        A = (Y*X_2 - X*XY)/delta;

        float tmpY;

        float y0 = A;
        entries.add(new Entry(y0, 0));

        float y_max = A+B* listaXY.get(listaXY.size()-1).get(0);
        entries.add(new Entry(y_max, iterator));

        LineDataSet set = new LineDataSet(entries, "Regresja liniowa");
        set.setColor(Color.rgb(0, 0, 155));
        LineData d = new LineData(warosciX, set);

        wynik.setText("Y = A + B*X, gdzie A = "+ A +" oraz B = " + B);

        return d;
    }


    private ScatterData generateScatterData(List<List<Float>> listaXY) {

        ArrayList<Entry> entries = new ArrayList<Entry>();
        ArrayList<String> warosciX = new ArrayList<String>();

        warosciX = wykresOsX(listaXY);

        int index = 0;
        List<Float> tempObject = new ArrayList<Float>();
        tempObject.add(0, 0f);
        tempObject.add(1, 0f);
        for (List<Float> object: listaXY)
        {
            if (object.get(0) == tempObject.get(0))
            {
                entries.add(new Entry(object.get(1), index));
            }
            else
            {
                index++;
                entries.add(new Entry(object.get(1), index));
                tempObject = object;

            }
        }

        ScatterDataSet set = new ScatterDataSet(entries, "Podane punkty");
        set.setColor(Color.rgb(155, 0, 0));
        set.setScatterShapeSize(7.5f);
        set.setDrawValues(false);
        set.setValueTextSize(10f);
        ScatterData d = new ScatterData(warosciX, set);

        return d;
    }

    public ArrayList<String> wykresOsX(List<List<Float>> listaXY)
    {
        ArrayList<String> warosciX = new ArrayList<String>();

        for (List<Float> object: listaXY)
        {
            warosciX.add(String.format("%.2f", object.get(0)));
        }
        return warosciX;
    }
}
