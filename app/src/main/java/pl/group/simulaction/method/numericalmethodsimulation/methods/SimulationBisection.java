package pl.group.simulaction.method.numericalmethodsimulation.methods;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import pl.group.simulaction.method.numericalmethodsimulation.R;

import static android.graphics.Color.BLUE;
import static android.graphics.Color.GREEN;

public class SimulationBisection extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    public static final int PLEASE_WAIT_DIALOG = 1;

    private EditText fromX;
    private EditText toX;
    private EditText epsilon;
    private EditText function;
    private Button setting;
    private TextView bisection_result;
    private ArrayList<Entry> function1 = new ArrayList<Entry>();
    private ArrayList<String> labels1 = new ArrayList<String>();
    private ArrayList<LineDataSet> lines = new ArrayList<LineDataSet>();

    private LineChart chart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_simulation_bisection);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        chart = (LineChart) findViewById(R.id.chart);
        setting = (Button) findViewById(R.id.accept);
        fromX = (EditText) findViewById(R.id.editTextFrom);
        toX = (EditText) findViewById(R.id.editTextTo);
        epsilon = (EditText) findViewById(R.id.editTextEpsilon);
        function = (EditText) findViewById(R.id.editTextFunction);
        function.setText("x * sin(x) - 1");
        fromX.setText("5");
        toX.setText("8");
        epsilon.setText("0.00001");
        bisection_result = (TextView) findViewById(R.id.wynik);
        setting.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.simulation_bisection, menu);
        return true;
    }

    @Override
    public Dialog onCreateDialog(int dialogId) {
        switch (dialogId) {
            case PLEASE_WAIT_DIALOG:
                ProgressDialog dialog = new ProgressDialog(this);
                dialog.setTitle("Obliczanie");
                dialog.setMessage("Proszę czekać...");
                dialog.setCancelable(true);

                return dialog;
            default:
                break;
        }

        return null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement


        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View v) {
        double a;
        double b;
        double e;
        try {
            a = Double.parseDouble(fromX.getText().toString());
            b = Double.parseDouble(toX.getText().toString());
            e = Double.parseDouble(epsilon.getText().toString());
        } catch (Exception ex) {
            Toast.makeText(this, "Wpisano liczbę w złym formacie!", Toast.LENGTH_SHORT).show();

            return;
        }

        Expression expression;
        try {
            String f = function.getText().toString();
            expression = new ExpressionBuilder(f).variables("x").build();
        } catch (Exception ex) {
            Toast.makeText(this, "Wpisano funkcję w złym formacie!", Toast.LENGTH_SHORT).show();

            return;
        }

        try {
            new Calculations(this, chart, expression, a, b, e).execute();
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public class Calculations extends AsyncTask<Void, Void, Void> {

        private Bisection bisection;
        private Activity triggeringActivity;
        private LineChart chart;

        private double result;
        private List<Bisection.Iteration> iterations;

        private String error;

        private Calculations(Activity triggeringActivity, LineChart chart,
                             Expression function, double a, double b, double e) {
            this.triggeringActivity = triggeringActivity;
            this.chart = chart;
            this.bisection = new Bisection(function, a, b, e);
        }

        @Override
        protected void onPreExecute() {
            triggeringActivity.showDialog(SimulationBisection.PLEASE_WAIT_DIALOG);
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            try {
                this.result = bisection.solve();
                this.iterations = bisection.getIterations();
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch(Exception e) {
                this.error = e.getMessage();
                Log.e("bisection", e.getMessage());
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            triggeringActivity.removeDialog(SimulationBisection.PLEASE_WAIT_DIALOG);
            DecimalFormat df = new DecimalFormat();
            df.setMaximumFractionDigits(2);
            df.setMinimumFractionDigits(2);
            this.chartBisection(df);
            if (this.error != null) {
                Toast.makeText(triggeringActivity, this.error, Toast.LENGTH_SHORT).show();
            } else {
                Bisection.Iteration it = this.iterations.get(this.iterations.size() - 1);
                String msg = "Obliczono! Wynik: x=" + it.x3 + " f(x)=" + it.fx3;
                Toast.makeText(triggeringActivity, msg, Toast.LENGTH_SHORT).show();
                bisection_result.setText(String.format( "Obliczono! Wynik: x= %.2f , f(x)= %.2f",it.x3, it.fx3));
            }
        }

        private double[] linspace(double min, double max, int points) {
            double[] d = new double[points];
            for (int i = 0; i < points; i++) {
                d[i] = min + i * (max - min) / (points - 1);
            }

            return d;
        }

        private void chartBisection(DecimalFormat df) {
            YAxis yAxis = chart.getAxisLeft();
            yAxis.setDrawGridLines(false);
            yAxis.setStartAtZero(false);
            XAxis xAxis = chart.getXAxis();
            xAxis.setDrawGridLines(false);
            xAxis.setSpaceBetweenLabels(1);
            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);

            function1.clear();
            labels1.clear();

            Bisection.Iteration it = iterations.get(0);
            double[] t = linspace(it.x1 - 2, it.x2 + 2, 100);

            List<Entry> entries = new ArrayList<>();
            List<String> xLabels = new ArrayList<>();
            for (int i = 0; i < t.length; i++) {
                double x = t[i];
                entries.add(new Entry((float)bisection.f(x), i));
                xLabels.add(df.format(x));
                function1.add(new Entry((float)bisection.f(x),i));
                labels1.add(String.valueOf((i)));
            }



            chart.setDescription("Wizualizacja metody Bisekji.");
            LineDataSet dataSet = new LineDataSet(function1, " ");
            dataSet.setColor(GREEN);
            lines.clear();
            lines.add(dataSet);
            LineData lineData = new LineData(labels1, lines);
            chart.setData(lineData);
            YAxis leftYAxis = chart.getAxisLeft();
            leftYAxis.setStartAtZero(false);


            LineDataSet dataSet1 = new LineDataSet(entries, "");
            dataSet.setColor(Color.rgb(0, 155, 0));
            dataSet.setLineWidth(2.5f);
            dataSet.setAxisDependency(YAxis.AxisDependency.LEFT);
            dataSet.setDrawValues(false);
            dataSet.setDrawCircles(false);

            this.chart.getLegend().setEnabled(false);
            this.chart.setDescription("");
            this.chart.setData(new LineData(xLabels, dataSet1));
            this.chart.invalidate();
        }
    }
}