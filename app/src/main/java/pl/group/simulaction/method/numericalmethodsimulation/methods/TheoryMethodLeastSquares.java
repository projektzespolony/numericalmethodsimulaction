package pl.group.simulaction.method.numericalmethodsimulation.methods;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import pl.group.simulaction.method.numericalmethodsimulation.R;

public class TheoryMethodLeastSquares extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_theory_method_least_squares);
    }
}
